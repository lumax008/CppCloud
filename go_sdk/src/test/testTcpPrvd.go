package main

import (
	"cppcloud"
	"fmt"
)

func main9() {
	appAttr := make(map[string]interface{})
	appAttr["svrid"] = 206
	appAttr["svrname"] = "go-tcp-prvd"
	capp := cppcloud.CreateCppCloudApp("cppcloud.cn:4800", appAttr, 3)
	capp.Start()

	prvd := cppcloud.CreateTCPProvider(capp, "", 2044)
	prvd.Start()

	fmt.Println("press any key to exit:")
	var str string
	fmt.Scanln(&str)
	fmt.Println("do exiting ..")
	capp.Shutdown() // 通知退出

	prvd.Join()
	fmt.Println("prvd end")
	capp.Join()
	fmt.Println("cloudapp end")
}
